/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Molnár Richárd
 */
public class GameTable implements Board {

    PlayerType[][] table = new PlayerType[3][3];
    public PlayerType playerType;
    public Cell cells;

    public GameTable() {

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                table[i][j] = PlayerType.EMPTY;

            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx > 2 || rowIdx < 0 || colIdx > 2 || colIdx < 0) {
            CellException cle = new CellException(rowIdx, colIdx, "kisebb számot adj meg");
            throw cle;
        }

        return playerType;
    }

    @Override
    public void put(Cell cell) throws CellException {
        if (table[cell.getRow()][cell.getCol()] == PlayerType.EMPTY){
            cell.setCellsPlayer(PlayerType.X);
            
        }else{
            CellException cle = new CellException(0, 0, "Itt már van jel, kerress másik mezőt");
            throw cle;
    }
            
        
    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean hasWon;
        if (this.table[0][0] == PlayerType.X && this.table[0][1] == PlayerType.X && this.table[0][2] == PlayerType.X) {
            hasWon = true;
            return hasWon;
        } else if (this.table[0][0] == PlayerType.O && this.table[0][1] == PlayerType.O && this.table[0][2] == PlayerType.O) {
            hasWon = true;
            return hasWon;
        } else if (this.table[1][0] == PlayerType.X && this.table[1][1] == PlayerType.X && this.table[1][2] == PlayerType.X) {
            hasWon = true;
            return hasWon;
        } else if (this.table[1][0] == PlayerType.O && this.table[1][1] == PlayerType.O && this.table[1][2] == PlayerType.O) {
            hasWon = true;
            return hasWon;
        } else if (this.table[2][0] == PlayerType.X && this.table[2][1] == PlayerType.X && this.table[2][2] == PlayerType.X) {
            hasWon = true;
            return hasWon;
        } else if (this.table[2][0] == PlayerType.O && this.table[2][1] == PlayerType.O && this.table[2][2] == PlayerType.O) {
            hasWon = true;
            return hasWon;
        } else if (this.table[0][0] == PlayerType.X && this.table[1][1] == PlayerType.X && this.table[2][2] == PlayerType.X) {
            hasWon = true;
            return hasWon;
        } else if (this.table[0][0] == PlayerType.O && this.table[1][1] == PlayerType.O && this.table[2][2] == PlayerType.O) {
            hasWon = true;
            return hasWon;
        } else if (this.table[2][0] == PlayerType.X && this.table[1][1] == PlayerType.X && this.table[0][2] == PlayerType.X) {
            hasWon = true;
            return hasWon;
        } else if (this.table[2][0] == PlayerType.O && this.table[1][1] == PlayerType.O && this.table[0][2] == PlayerType.O) {
            hasWon = true;
            return hasWon;
        } else {
            return false;
        }

    }

    @Override
    public List<Cell> emptyCells() {

        List<Cell> empc;
        empc = new ArrayList<>();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table[i][j] == PlayerType.EMPTY) {
                   empc.add(new Cell (i, j));
                    
                }
                
            }
            
        }return empc;
        }
        
    }


